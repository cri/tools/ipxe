#!/bin/sh

SCRIPT="${1:-cri}.ipxe"

build() {
    target="$1"
    script="$2"

    make -C src -j 9 "${target}" EMBED="${script}" \
      TRUST=../certs/le-x3.pem,../certs/ipxe.ca.pem \
      CERT=../certs/le-x3.pem \
      CERT=../certs/ipxe.ca.pem
}

for target in "bin/undionly.kpxe" \
              "bin/ipxe.iso" \
              "bin-x86_64-efi/ipxe.efi" \
              "bin-x86_64-efi/ipxe.iso" \
              "bin-x86_64-efi/snp.efi"
do
    build "${target}" "../scripts/${SCRIPT}"
done
