/*
 * Banner timeout configuration
 *
 * This controls the timeout for the "Press Ctrl-B for the iPXE
 * command line" banner displayed when iPXE starts up.  The value is
 * specified in tenths of a second for which the banner should appear.
 * A value of 0 disables the banner.
 */
#undef BANNER_TIMEOUT
#define BANNER_TIMEOUT		0

#define NET_PROTO_IPV6		/* IPv6 protocol */
#define NET_PROTO_LLDP		/* LLDP protocol */

#define DOWNLOAD_PROTO_FILE	/* Secure Hypertext Transfer Protocol */
#define DOWNLOAD_PROTO_HTTPS	/* Secure Hypertext Transfer Protocol */

#define CERT_CMD		/* Certificate management commands */
#define CONSOLE_CMD		/* Console command */
#define DIGEST_CMD		/* Image crypto digest commands */
#define IMAGE_TRUST_CMD		/* Image trust management commands */
#define IPSTAT_CMD
#define NEIGHBOUR_CMD		/* Neighbour management commands */
#define NSLOOKUP_CMD		/* DNS resolving command */
#define NTP_CMD			/* NTP commands */
#define PARAM_CMD		/* Form parameter commands */
#define PCI_CMD
#define PING_CMD		/* Ping command */
#define POWEROFF_CMD		/* Power off command */
#define REBOOT_CMD		/* Reboot command */
#define TIME_CMD		/* Time commands */
#define VLAN_CMD		/* VLAN commands */
#define WOL_CMD
